package com.example.learn;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Cards extends AppCompatActivity {
CardView car_letter,car_numer,car_quran;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cards);
        car_letter=findViewById(R.id.car_letter);
        car_numer=findViewById(R.id.car_num);
        car_quran=findViewById(R.id.car_qurane);
        car_letter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(Cards.this,Letter.class);
                startActivity(i);
            }
        });
        car_numer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(Cards.this,number.class);
                startActivity(i);
            }
        });
        car_quran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(Cards.this,Quran.class);
                startActivity(i);
            }
        });
    }
}
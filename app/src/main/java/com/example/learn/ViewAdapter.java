package com.example.learn;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ViewAdapter extends PagerAdapter {
    private Context context;
    private Object ValueEventListener;
    FirebaseDatabase data_base=FirebaseDatabase.getInstance();
    DatabaseReference db_ref= data_base.getReference("frist");
     /*   db_ref.addValueEventListener(new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot snapshot) {

        }

        @Override
        public void onCancelled(@NonNull DatabaseError error) {

        }
    });*/

    private LayoutInflater layoutInflater;
    private String[] text={"a","b"," c"} ;
    ViewAdapter(Context context){
        this.context=context;
    }
    @Override
    public int getCount() {
        return text.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view=layoutInflater.inflate(R.layout.item,null);
        TextView quran = view.findViewById(R.id.textView3);
        quran.setText(text[position]);
        ViewPager viewPager= (ViewPager) container;
        viewPager.addView(view,0);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        ViewPager viewPager= (ViewPager) container;
        View view =(View) object;
        viewPager.removeView(view);
    }
}

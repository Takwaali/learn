package com.example.learn;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Signup extends AppCompatActivity {
TextView tv_login;
    EditText user_name,password,confirmpass;
    Button bt_signup;
    DBHelper DB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        user_name=findViewById(R.id.editText);
        password=findViewById(R.id.editText4);
        confirmpass=findViewById(R.id.editText5);
        bt_signup=findViewById(R.id.bt_signup);
        tv_login=findViewById(R.id.tv_login);
        DB =new DBHelper(this);
        tv_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(Signup.this,Login.class);
                startActivity(i);
            }
        });
        bt_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user =user_name.getText().toString();
                String pass= password.getText().toString();
                String conpass= confirmpass.getText().toString();
                if (user.equals("")||pass.equals("")||conpass.equals(""))
                {
                    Toast.makeText(Signup.this, "ادخل جميع البيانات", Toast.LENGTH_SHORT).show();
                }
                else {
                    if (pass.equals(conpass))
                    {
                        Boolean checkuser=DB.checkusername(user);
                        if (checkuser==false){
                            Boolean insert =DB.insertData(user,pass);
                            if (insert==true){
                                Toast.makeText(Signup.this, "تم التسجيل بنجاح", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            }
        });
    }
}
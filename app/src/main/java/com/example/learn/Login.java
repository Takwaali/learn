package com.example.learn;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Login extends AppCompatActivity {
TextView tv_signup;
Button bt_login;
EditText user_name,password;
DBHelper DB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        bt_login=findViewById(R.id.bt_login);
        user_name=findViewById(R.id.editText);
        password=findViewById(R.id.editText4);
        tv_signup=findViewById(R.id.tv_signup);
        DB =new DBHelper(this);
        tv_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(Login.this,Signup.class);
                startActivity(i);
            }
        });
        bt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
          String user =user_name.getText().toString();
          String pass=password.getText().toString();
          if (user.equals("")||pass.equals("")){
              Toast.makeText(Login.this, "ادخل كل البيانات", Toast.LENGTH_SHORT).show();
          }
          else {

              Boolean checkuserpass = DB.checkusernamepassward(user,pass);
              if (checkuserpass==true){
                  Toast.makeText(Login.this, "تم الدخول", Toast.LENGTH_SHORT).show();
                  Intent i=new Intent(Login.this,Cards.class);
                  startActivity(i);
              }
              else {
                  Toast.makeText(Login.this, "تسجيل غير صحيح ", Toast.LENGTH_SHORT).show();
              }
          }
            }
        });
    }
}
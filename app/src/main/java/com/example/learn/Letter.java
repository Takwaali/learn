package com.example.learn;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Letter extends AppCompatActivity {
    TextView letter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_letter);

        letter =findViewById(R.id.letter);

        FirebaseDatabase data_base=FirebaseDatabase.getInstance();
        DatabaseReference db_ref= data_base.getReference().child("litter");
        db_ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

              if (snapshot.exists()){

                  String data=snapshot.getValue().toString();
                  letter.setText(data);
                  Log.d("takwa", " code= "+data);
              }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.d("takwa", " code= "+error.toException());
            }
        });

    }
}